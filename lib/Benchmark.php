<?php 

class Benchmark {
	
	public static function createField($type, $index, $settings=null) {
		$name = "benchmark_{$type}_{$index}";
		
		if (!$settings) {
			switch ($type) {
				case 'text':
					$settings = array(
						'max_length' => 60
					);
					break;
			}
		}
		
		$field = array(
			'field_name' => $name,
			'cardinality' => 1,
			'type' => $type,
			'settings' => $settings,
		);
		
		field_create_field($field);
		
		// create instance for this field
		$instance = array(
			'entity_type' => 'node',
			'bundle' => 'fields_benchmark',
		
			'field_name' => $name,
			'label' => $name,
			'widget' => array(
				'type' => 'text_textfield'
			),
		);
		
		field_create_instance($instance);
	}
}
